<script setup>
import BtnNaughty from '../../src/components/btn-naughty/btn-naughty.vue';

import WrapperStereoscopic from '../../src/components/wrapper-stereoscopic/wrapper-stereoscopic.vue';
import WrapperStereoscopicLayer from '../../src/components/wrapper-stereoscopic/wrapper-stereoscopic-layer.vue';

import WrapperPhysics from '../../src/components/wrapper-physics/examples/basic-usage.vue';
import WrapperCatEar from '../../src/components/wrapper-cat-ear/examples/interactive-effect.vue';
import WrapperKirbyMouthfulMode from '../../src/components/wrapper-kirby-mouthful-mode/examples/basic-usage.vue';

import UtilPartyPopper from '../../src/components/util-party-popper/examples/emit-position.vue';

</script>

# 元件清單

## 按鈕

經典卻又不常見的按鈕。(。・∀・)ノ

### [調皮的按鈕](./btn-naughty/) <Badge type="info" text="button" />

一個停用時會越嚕越遠的按鈕，像極了你家那隻欠揍的貓。(._.`)

<div class="flex w-full justify-center py-[20vh]">
  <btn-naughty label="(^._.^)ﾉ" z-index="99" disabled/>
</div>

## 包裝器

包起來，產生各種有趣的效果吧！(´▽`ʃ♡ƪ)

### [立體包裝器](./wrapper-stereoscopic/) <Badge type="info" text="wrapper" />

可以讓元素有酷酷的 3D 偏轉效果

<div class="flex">
  <wrapper-stereoscopic v-slot="wrapper">
  <div
    class=" p-10 border rounded flex-center"
    :style="wrapper.style"
  >
    <wrapper-stereoscopic-layer v-slot="layer01">
      <div
        class=" p-10 border rounded flex-center"
        :style="layer01.style"
      >
        <wrapper-stereoscopic-layer v-slot="layer02">
          <div class="text-xl font-bold">
            ( ﾟ ∀。) 好暈好暈啊
          </div>
        </wrapper-stereoscopic-layer>
      </div>
    </wrapper-stereoscopic-layer>
  </div>
</wrapper-stereoscopic>
</div>

### [物理包裝器](./wrapper-physics/) <Badge type="info" text="wrapper" />

產生物理世界，讓內部元素具有物理效果

<wrapper-physics />

### [貓耳包裝器](./wrapper-cat-ear/) <Badge type="info" text="wrapper" />

任何元素包起來就會長出貓耳，讓萬物皆可萌吧！(^・ω・^ )

<wrapper-cat-ear class="h-[70vh]" />

### [塞滿嘴包裝器](./wrapper-kirby-mouthful-mode/) <Badge type="info" text="wrapper" />

讓粉紅惡魔來幫你吃掉畫面上的一切。( ͡• ͜ʖ ͡• )

<wrapper-kirby-mouthful-mode class="h-[70vh]" />

## 實用

### [拉炮](./util-party-popper/) <Badge type="info" text="util" />

隨時隨地都可以慶祝！✧｡٩(ˊᗜˋ*)و✧*｡

<util-party-popper class="h-[70vh]" />
